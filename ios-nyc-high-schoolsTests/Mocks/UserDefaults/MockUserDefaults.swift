//
//  MockUserDefaults.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import Foundation
@testable import ios_nyc_high_schools

class MockUserDefaults: UserDefaultsProtocol {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private var result: Bool = false
    
    func bool(forKey defaultName: String) -> Bool {
        return result
    }
    
    func set(_ value: Bool, forKey defaultName: String) {
        result = value
    }
}
