//
//  MockUserDefaultsService.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import Foundation
@testable import ios_nyc_high_schools

class MockUserDefaultsService: UserDefaultsServiceProtocol {
    
    private var result: Bool = false
    private(set) var userHasInteractedwithOnboardingCallCount: Int = 0
    
    var hasUserInteractedWithOnboarding: Bool {
        return result
    }
    
    func userHasInteractedwithOnboarding() {
        result = true
        userHasInteractedwithOnboardingCallCount += 1
    }
}
