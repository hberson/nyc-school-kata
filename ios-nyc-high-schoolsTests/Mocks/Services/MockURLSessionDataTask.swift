//
//  MockURLSessionDataTask.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import Foundation

class MockURLSessionDataTask: URLSessionDataTask {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private let handler: () -> Void
    
    init(handler: @escaping () -> Void) {
        self.handler = handler
    }
    
    override func resume() {
        handler()
    }
}
