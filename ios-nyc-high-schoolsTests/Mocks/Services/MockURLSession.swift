//
//  MockURLSession.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import Foundation
@testable import ios_nyc_high_schools

class MockURLSession: URLSessionProtocol {
    typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void
    //------------------------
    // MARK: - Member Variables
    //------------------------
    var data: Data?
    var error: Error?
    
    //------------------------
    // MARK: - Helpers
    //------------------------
    func dataTask(with url: URL, completionHandler: @escaping CompletionHandler) -> URLSessionDataTask {
        let data = self.data
        let error = self.error
        return MockURLSessionDataTask {
            completionHandler(data, nil, error)
        }
    }
}
