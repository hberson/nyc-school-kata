//
//  MockLoadingOverlayDelegate.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import Foundation
@testable import ios_nyc_high_schools

class MockLoadingOverlayDelegate: LoadingOverlayViewControllerCoordinatorDelegate {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private(set) var coordinator: LoadingOverlayViewControllerCoordinator! = nil
    private(set) var loadingOverlayViewControllerCoordinatorRequestDimissCallCount: Int = 0
    
    func loadingOverlayViewControllerCoordinatorRequestDimiss(_ coordinator: LoadingOverlayViewControllerCoordinator) {
        self.coordinator = coordinator
        loadingOverlayViewControllerCoordinatorRequestDimissCallCount += 1
    }
}
