//
//  MockOnboardDelegate.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import Foundation
@testable import ios_nyc_high_schools

class MockOnboardDelegate: OnboardViewControllerDelegate {
    
    private(set) var onboardViewControllerDidTapOkGotItCallCount: Int = 0
    private(set) var viewController: OnboardViewController! = nil
    
    func onboardViewControllerDidTapOkGotIt(_ viewController: OnboardViewController) {
        self.viewController = viewController
        onboardViewControllerDidTapOkGotItCallCount += 1
    }
}
