//
//  MockSchoolListSelectionFlowDelegate.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/27/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import Foundation
@testable import ios_nyc_high_schools

class MockSchoolListSelectionFlowDelegate: SchoolListSelectionFlowDelegate {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private(set) var schoolListDelegateDidSelectCallCount: Int = 0
    private(set) var delegate: SchoolListDelegate?
    private(set) var school: School?
    
    //------------------------
    // MARK: - Helpers
    //------------------------
    func schoolListDelegateDidSelect(_ delegate: SchoolListDelegate, school: School) {
        schoolListDelegateDidSelectCallCount += 1
        self.delegate = delegate
        self.school = school
    }
}
