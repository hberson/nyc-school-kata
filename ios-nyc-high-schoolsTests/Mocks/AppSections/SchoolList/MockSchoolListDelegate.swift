//
//  MockSchoolListDelegate.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import Foundation
@testable import ios_nyc_high_schools

class MockSchoolListDelegate: SchoolListViewControllerDelegate {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private(set) var schoolListViewControllerFetchLastestInfoCallCount: Int = 0
    private(set) var schoolListViewControllerPresentDetailViewCallCount: Int = 0
    private(set) var viewController: SchoolListViewController?
    private(set) var school: School?
    
    //------------------------
    // MARK: - Helpers
    //------------------------
    func schoolListViewControllerFetchLastestInfo(_ viewController: SchoolListViewController) {
        self.viewController = viewController
        schoolListViewControllerFetchLastestInfoCallCount += 1
    }
    
    func schoolListViewControllerPresentDetailView(_ viewController: SchoolListViewController, school: School) {
        schoolListViewControllerPresentDetailViewCallCount += 1
        self.viewController = viewController
        self.school = school
    }
}
