//
//  MockNavigationController.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit
@testable import ios_nyc_high_schools

class MockNavigationController: NavigationController {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private(set) var viewControllerToPresent: UIViewController?
    private var viewController: UIViewController?
    private var navigationBarIsHidden: Bool = false
    
    func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)?) {
        self.viewControllerToPresent = viewControllerToPresent
    }
    
    func pushViewController(_ viewController: UIViewController, animated: Bool) {
        self.viewController = viewController
    }
    
    func setNavigationBarHidden(_ hidden: Bool, animated: Bool) {
        navigationBarIsHidden = hidden
    }
    
    var interactivePopGestureRecognizer: UIGestureRecognizer?
    
    var viewControllers: [UIViewController] {
        return []
    }
}
