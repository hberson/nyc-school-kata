//
//  SchoolServiceTests.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import XCTest
@testable import ios_nyc_high_schools

class SchoolServiceTests: XCTestCase {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private var subject: SchoolService!
    private var urlSession: MockURLSession!
    
    //------------------------
    // MARK: - Lifecycle
    //------------------------
    override func setUp() {
        super.setUp()
        urlSession = MockURLSession()
        subject = SchoolService(session: urlSession)
    }

    override func tearDown() {
        subject = nil
        urlSession = nil
        super.tearDown()
    }
    
    func testMakeRequest_SuccessfulSchoolResponse() {
        urlSession.data = buildSchoolListData()
        let expectation = self.expectation(description: "School List Network Response")
        var networkResult: ServiceResponse<[School]>?
        
        subject.makeRequest(for: "maryhadalittlelamb") { (result: ServiceResponse<[School]>) in
            networkResult = result
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        guard let result = networkResult else {
            assertionFailure("Expected success result object, but got nil", file: #file, line: #line)
            return
        }
        
        switch result {
        case let .success(schools):
            let school = schools.first
            XCTAssertEqual(school?.dbn, "02M260")
            XCTAssertEqual(school?.schoolName, "Clinton School Writers & Artists, M.S. 260")
            XCTAssertEqual(school?.primaryAddressLine1, "10 East 15th Street")
            XCTAssertEqual(school?.city, "Manhattan")
            XCTAssertEqual(school?.zip, "10003")
            XCTAssertEqual(school?.stateCode, "NY")
        default:
            break
        }
    }
    
    func testMakeRequest_SuccessfulScoreResponse() {
        urlSession.data = buildScoreData()
        let expectation = self.expectation(description: "Scores Network Response")
        var networkResult: ServiceResponse<[Score]>?
        
        subject.makeRequest(for: "littlelamb") { (result: ServiceResponse<[Score]>) in
            networkResult = result
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        guard let result = networkResult else {
            assertionFailure("Expected success result object, but got nil", file: #file, line: #line)
            return
        }
        
        switch result {
        case let .success(scores):
            let score = scores.first
            XCTAssertEqual(score?.dbn, "01M292")
            XCTAssertEqual(score?.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
            XCTAssertEqual(score?.numOfSatTestTakers, "29")
            XCTAssertEqual(score?.satCriticalReadingAvgScore, "355")
            XCTAssertEqual(score?.satMathAvgScore, "404")
            XCTAssertEqual(score?.satWritingAvgScore, "363")
        default:
            break
        }
    }
    
    func testMakeRequest_FailureForInvalidPath() {
        let expectation = self.expectation(description: "Invalid URL")
        var networkResult: ServiceResponse<[Score]>?
        
        subject.makeRequest(for: "") { (result: ServiceResponse<[Score]>) in
            networkResult = result
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        guard let result = networkResult else {
            assertionFailure("Expected success result object, but got nil", file: #file, line: #line)
            return
        }
        
        switch result {
        case let .error(error):
            XCTAssertEqual(error.localizedDescription, "Invalid url path, please try again.")
        default:
            break
        }
    }
    
    func testMakeRequest_FailureForBadNetworkResponse() {
        urlSession.data = Data([0, 1, 0, 1])
        let expectation = self.expectation(description: "Bad Network Response")
        var networkResult: ServiceResponse<[Score]>?
        
        subject.makeRequest(for: "littlelamb") { (result: ServiceResponse<[Score]>) in
            networkResult = result
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        
        guard let result = networkResult else {
            assertionFailure("Expected success result object, but got nil", file: #file, line: #line)
            return
        }
        
        switch result {
        case let .error(error):
            XCTAssertEqual(error.localizedDescription, "The data couldn’t be read because it isn’t in the correct format.")
        default:
            break
        }
    }
    
    //------------------------
    // MARK: - Helpers
    //------------------------
    private func buildSchoolListData() -> Data? {
        let response: [[String: Any]] = [
            ["dbn": "02M260",
            "school_name": "Clinton School Writers & Artists, M.S. 260",
            "primary_address_line_1": "10 East 15th Street",
            "city": "Manhattan",
            "zip": "10003",
            "state_code": "NY"]
        ]
        
        do {
            let data = try JSONSerialization.data(withJSONObject: response, options: JSONSerialization.WritingOptions.prettyPrinted)
            return data
        } catch {
            assertionFailure("Could not build data from response \(response)")
        }
        
        return nil
    }
    
    private func buildScoreData() -> Data? {
        let response: [[String: Any]] = [
            ["dbn": "01M292",
             "school_name": "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
             "num_of_sat_test_takers": "29",
             "sat_critical_reading_avg_score": "355",
             "sat_math_avg_score": "404",
             "sat_writing_avg_score": "363"]
        ]
        
        do {
            let data = try JSONSerialization.data(withJSONObject: response, options: JSONSerialization.WritingOptions.prettyPrinted)
            return data
        } catch {
            assertionFailure("Could not build data from response \(response)")
        }
        
        return nil
    }
}
