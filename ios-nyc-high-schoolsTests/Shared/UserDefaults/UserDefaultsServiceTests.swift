//
//  UserDefaultsServiceTests.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import XCTest
@testable import ios_nyc_high_schools

class UserDefaultsServiceTests: XCTestCase {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private var subject: UserDefaultsService! = nil
    private var userDefaults: MockUserDefaults = MockUserDefaults()
    
    override func setUp() {
        super.setUp()
        subject = UserDefaultsService(userDefaults: userDefaults)
    }

    override func tearDown() {
        subject = nil
        super.tearDown()
    }
    
    func test_hasUserInteractedWithOnboarding_returns_false() {
        XCTAssertFalse(subject.hasUserInteractedWithOnboarding)
    }
    
    func test_hasUserInteractedWithOnboarding_returns_true() {
        subject.userHasInteractedwithOnboarding()
        
        XCTAssertTrue(subject.hasUserInteractedWithOnboarding)
    }
}
