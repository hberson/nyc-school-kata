//
//  OnboardViewControllerTests.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import XCTest
@testable import ios_nyc_high_schools

class OnboardViewControllerTests: XCTestCase {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private var subject: OnboardViewController! = nil
    private var userDefaultsService: MockUserDefaultsService!
    private var delegate: MockOnboardDelegate!
    
    //------------------------
    // MARK: - Lifecycle
    //------------------------
    override func setUp() {
        super.setUp()
        userDefaultsService = MockUserDefaultsService()
        delegate = MockOnboardDelegate()
        
        subject = UIStoryboard.init(identifier: .onboard).instantiateViewController()
        subject.userDefaultsService = userDefaultsService
        subject.delegate = delegate
        
        _ = subject.view
    }

    override func tearDown() {
        subject = nil
        userDefaultsService = nil
        delegate = nil
        super.tearDown()
    }
    
    func testOkGotItButtonAction_SetsUserHasInteractedWithOnboardingToTrue() {
        subject.okButton.sendActions(for: .touchUpInside)
        
        XCTAssertTrue(userDefaultsService.hasUserInteractedWithOnboarding, "Expected to equal true, but got \(userDefaultsService.hasUserInteractedWithOnboarding)")
        XCTAssertEqual(userDefaultsService.userHasInteractedwithOnboardingCallCount, 1, "Expected userHasInteractedwithOnboarding to be called 1 time, but got \(userDefaultsService.userHasInteractedwithOnboardingCallCount) times.")
    }
    
    func testOkGotItButtonAction_InvokesOnboardViewControllerDidTapGotItOnItsDelegate() {
        subject.okButton.sendActions(for: .touchUpInside)
        
        XCTAssertEqual(delegate.viewController, subject)
        XCTAssertEqual(delegate.onboardViewControllerDidTapOkGotItCallCount, 1, "Expected userHasInteractedwithOnboarding to be called 1 time, but got \(delegate.onboardViewControllerDidTapOkGotItCallCount) times.")
    }
}
