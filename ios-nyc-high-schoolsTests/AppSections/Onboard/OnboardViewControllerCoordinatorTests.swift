//
//  OnboardViewControllerCoordinatorTests.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import XCTest
@testable import ios_nyc_high_schools

class OnboardViewControllerCoordinatorTests: XCTestCase {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private var subject: OnboardViewControllerCoordinator! = nil
    private var window: UIWindow! = UIWindow()
    
    override func setUp() {
        super.setUp()
        subject = OnboardViewControllerCoordinator(window: window)
    }

    override func tearDown() {
        subject = nil
        window = nil
        super.tearDown()
    }
    
    func test_whenOnboardViewControllerDidTapOkGotIt_thenTheRootViewControllerIsSchoolListViewController() {
        subject.onboardViewControllerDidTapOkGotIt(OnboardViewController())
        
        let navigationViewController = window.rootViewController as? UINavigationController
        let topViewController = navigationViewController?.topViewController
        
        XCTAssertTrue(topViewController is SchoolListViewController)
    }
}
