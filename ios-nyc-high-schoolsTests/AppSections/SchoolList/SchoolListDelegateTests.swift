//
//  SchoolListDelegateTests.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/27/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import XCTest
@testable import ios_nyc_high_schools

class SchoolListDelegateTests: XCTestCase {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private var subject: SchoolListDelegate!
    private var delegate: MockSchoolListSelectionFlowDelegate?
    //------------------------
    // MARK: - Lifecycle
    //------------------------
    override func setUp() {
        super.setUp()
        delegate = MockSchoolListSelectionFlowDelegate()
        subject = SchoolListDelegate(schools: [], scores: [])
        subject.delegate = delegate
    }

    override func tearDown() {
        subject = nil
        delegate = nil
        super.tearDown()
    }
    
    //------------------------
    // MARK: - Tests
    //------------------------
    func testDidSelectRowAt_ReturnsAndDoesNotInvokeTheDelegate_GivenSchoolsArrayIsEmtpy() {
        subject.tableView(UITableView(), didSelectRowAt: IndexPath(row: 0, section: 0))
        
        XCTAssert(delegate?.schoolListDelegateDidSelectCallCount == 0)
        XCTAssertNil(delegate?.school)
    }
    
    func testDidSelectRowAt_DoesNotSetScoreObjectOnTheSchool_GivenThatTheDataDoesNotExist() {
        let schools: [School] = [
            School(dbn: "123456", schoolName: nil, overviewParagraph: nil, academicopportunities1: nil, academicopportunities2: nil, ellPrograms: nil, neighborhood: nil, phoneNumber: nil, faxNumber: nil, schoolEmail: nil, website: nil, totalStudents: nil, extracurricularActivities: nil, schoolSports: nil, primaryAddressLine1: nil, city: nil, zip: nil, stateCode: nil, score: nil)
        ]
        
        subject = SchoolListDelegate(schools: schools, scores: [])
        subject.delegate = delegate
        
        subject.tableView(UITableView(), didSelectRowAt: IndexPath(row: 0, section: 0))
        XCTAssertNotNil(delegate?.school)
        XCTAssertNil(delegate?.school?.score)
    }
    
    func testDidSelectRowAt_DoesSetsTheScoreObjectOnTheSchool() {
        let schools: [School] = [
            School(dbn: "123456", schoolName: nil, overviewParagraph: nil, academicopportunities1: nil, academicopportunities2: nil, ellPrograms: nil, neighborhood: nil, phoneNumber: nil, faxNumber: nil, schoolEmail: nil, website: nil, totalStudents: nil, extracurricularActivities: nil, schoolSports: nil, primaryAddressLine1: nil, city: nil, zip: nil, stateCode: nil, score: nil)
        ]
        
        let scores: [Score] = [
            Score(dbn: "741258", schoolName: nil, numOfSatTestTakers: nil, satCriticalReadingAvgScore: nil, satMathAvgScore: nil, satWritingAvgScore: nil),
            Score(dbn: "963258", schoolName: nil, numOfSatTestTakers: nil, satCriticalReadingAvgScore: nil, satMathAvgScore: nil, satWritingAvgScore: nil),
            Score(dbn: "852456", schoolName: nil, numOfSatTestTakers: nil, satCriticalReadingAvgScore: nil, satMathAvgScore: nil, satWritingAvgScore: nil),
            Score(dbn: "123456", schoolName: nil, numOfSatTestTakers: nil, satCriticalReadingAvgScore: nil, satMathAvgScore: nil, satWritingAvgScore: nil)
        ]
        
        subject = SchoolListDelegate(schools: schools, scores: scores)
        subject.delegate = delegate
        
        subject.tableView(UITableView(), didSelectRowAt: IndexPath(row: 0, section: 0))
        XCTAssertNotNil(delegate?.school)
        XCTAssertNotNil(delegate?.school?.score)
    }
    
    func testDidSelectRowAt_InvokesTheDelegate() {
        let schools: [School] = [
            School(dbn: "123456", schoolName: nil, overviewParagraph: nil, academicopportunities1: nil, academicopportunities2: nil, ellPrograms: nil, neighborhood: nil, phoneNumber: nil, faxNumber: nil, schoolEmail: nil, website: nil, totalStudents: nil, extracurricularActivities: nil, schoolSports: nil, primaryAddressLine1: nil, city: nil, zip: nil, stateCode: nil, score: nil)
        ]
        
        subject = SchoolListDelegate(schools: schools, scores: [])
        subject.delegate = delegate
        
        subject.tableView(UITableView(), didSelectRowAt: IndexPath(row: 0, section: 0))
        XCTAssert(delegate?.schoolListDelegateDidSelectCallCount == 1)
    }
}
