//
//  SchoolListDataSourceTests.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/27/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import XCTest
@testable import ios_nyc_high_schools

class SchoolListDataSourceTests: XCTestCase {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private var subject: SchoolListDataSource!
    private var schools: [School]! = []

    //------------------------
    // MARK: - Lifecycle
    //------------------------
    override func tearDown() {
        subject = nil
        schools = nil
        super.tearDown()
    }
    
    //------------------------
    // MARK: - Helpers
    //------------------------
    func testNumberOfSections_ReturnsZero_GivenTheSchoolsArrayIsEmpty() {
        subject = SchoolListDataSource(schools: schools)
        
        let numberOfSections = subject.numberOfSections(in: UITableView())
        
        XCTAssert(numberOfSections == 0)
    }
    
    func testNumberOfSections_ReturnsOne_GivenTheSchoolsArrayIsNotEmpty() {
        let school: School = School(dbn: "123456", schoolName: nil, overviewParagraph: nil, academicopportunities1: nil, academicopportunities2: nil, ellPrograms: nil, neighborhood: nil, phoneNumber: nil, faxNumber: nil, schoolEmail: nil, website: nil, totalStudents: nil, extracurricularActivities: nil, schoolSports: nil, primaryAddressLine1: nil, city: nil, zip: nil, stateCode: nil, score: nil)
        
        schools = [school]
        
        subject = SchoolListDataSource(schools: schools)
        
        let numberOfSections = subject.numberOfSections(in: UITableView())
        
        XCTAssert(numberOfSections == 1)
    }
    
    func testNumberOfRowsInSection_ReturnsZero_GivenTheSchoolsArrayIsEmpty() {
        subject = SchoolListDataSource(schools: schools)
        
        let numberOfRowsSection = subject.tableView(UITableView(), numberOfRowsInSection: 0)
        
        XCTAssert(numberOfRowsSection == 0)
    }
    
    func testNumberOfRowsInSection_ReturnsTheCorrectCount_GivenTheSchoolsArrayIsNotEmpty() {
        let school: School = School(dbn: "123456", schoolName: nil, overviewParagraph: nil, academicopportunities1: nil, academicopportunities2: nil, ellPrograms: nil, neighborhood: nil, phoneNumber: nil, faxNumber: nil, schoolEmail: nil, website: nil, totalStudents: nil, extracurricularActivities: nil, schoolSports: nil, primaryAddressLine1: nil, city: nil, zip: nil, stateCode: nil, score: nil)
        
        schools = [school]
        
        subject = SchoolListDataSource(schools: schools)
        
        let numberOfRowsSection = subject.tableView(UITableView(), numberOfRowsInSection: 0)
        
        XCTAssert(numberOfRowsSection == 1)
    }
}
