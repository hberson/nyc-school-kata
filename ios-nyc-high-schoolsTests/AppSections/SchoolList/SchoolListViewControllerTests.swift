//
//  SchoolListViewControllerTests.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import XCTest
@testable import ios_nyc_high_schools

class SchoolListViewControllerTests: XCTestCase {

    private var subject: SchoolListViewController!
    private var delegate: MockSchoolListDelegate!
    
    override func setUp() {
        super.setUp()
        delegate = MockSchoolListDelegate()
        
        subject = UIStoryboard(identifier: .schoolList).instantiateViewController()
        subject.delegate = delegate
        
        _ = subject.view
    }

    override func tearDown() {
        subject = nil
        delegate = nil
        super.tearDown()
    }
    
    func testViewDidLoad_InvokesSchoolListViewControllerFetchLatestInfo() {
        XCTAssertTrue(delegate.schoolListViewControllerFetchLastestInfoCallCount == 1, "Expected schoolListViewControllerFetchLastestInfo to be called 1 time, but got called \(delegate.schoolListViewControllerFetchLastestInfoCallCount) times.")
        XCTAssertEqual(delegate.viewController, subject)
    }
    
    func testViewDidLoad_SetsTheCorrectHeightToTheTableViewRow() {
        XCTAssert(subject.tableView.rowHeight == 60)
    }
    
    func testServiceRequestCompleted_SetTheSubjectAsTheDelegateForTheSchoolListDelegate() {
        subject.SchoolListViewControllerCoordinatorServiceRequestCompleted(schools: [], scores: [])
        
        XCTAssert(subject.schoolListDelegate?.delegate is SchoolListViewController)
    }
    
    func testServiceRequestCompleted_SetTheCorrectDatasourceForTheTableView() {
        subject.SchoolListViewControllerCoordinatorServiceRequestCompleted(schools: [], scores: [])
        
        XCTAssert(subject.tableView.dataSource is SchoolListDataSource)
    }
    
    func testServiceRequestCompleted_SetTheCorrectDelegateForTheTableView() {
        subject.SchoolListViewControllerCoordinatorServiceRequestCompleted(schools: [], scores: [])
        
        XCTAssert(subject.tableView.delegate is SchoolListDelegate)
    }
    
    func testschoolListDelegateDidSelect_InvokesTheDelegateToPresentTheDetailView() {
        let score = Score(dbn: "123456", schoolName: nil, numOfSatTestTakers: nil, satCriticalReadingAvgScore: nil, satMathAvgScore: nil, satWritingAvgScore: nil)
        
        let school = School(dbn: "123456", schoolName: nil, overviewParagraph: nil, academicopportunities1: nil, academicopportunities2: nil, ellPrograms: nil, neighborhood: nil, phoneNumber: nil, faxNumber: nil, schoolEmail: nil, website: nil, totalStudents: nil, extracurricularActivities: nil, schoolSports: nil, primaryAddressLine1: nil, city: nil, zip: nil, stateCode: nil, score: score)
        
        subject.schoolListDelegateDidSelect(SchoolListDelegate(schools: [school], scores: [score]), school: school)
        
        XCTAssert(delegate.school?.dbn == school.dbn)
        XCTAssert(delegate.school?.score?.dbn == school.score?.dbn)
        XCTAssert(delegate.schoolListViewControllerPresentDetailViewCallCount == 1)
    }
}
