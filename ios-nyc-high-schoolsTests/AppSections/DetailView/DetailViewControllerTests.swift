//
//  DetailViewControllerTests.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import XCTest
@testable import ios_nyc_high_schools

class DetailViewControllerTests: XCTestCase {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private var subject: DetailViewController!
    private var school: School?
    private var score: Score?
    
    override func setUp() {
        super.setUp()
        score = Score(dbn: "123456", schoolName: "Elmo's School", numOfSatTestTakers: "55", satCriticalReadingAvgScore: "255", satMathAvgScore: "7456", satWritingAvgScore: "power level over 9000")
        
        school = School(dbn: "123456", schoolName: "Elmo's School", overviewParagraph: "Hey, hey, hey... it's fat albert.", academicopportunities1: nil, academicopportunities2: nil, ellPrograms: nil, neighborhood: nil, phoneNumber: nil, faxNumber: nil, schoolEmail: nil, website: nil, totalStudents: nil, extracurricularActivities: nil, schoolSports: nil, primaryAddressLine1: nil, city: nil, zip: nil, stateCode: nil, score: score)
        
        subject = UIStoryboard(identifier: .detailView).instantiateViewController()
        subject.school = school
        
        _ = subject.view
    }

    override func tearDown() {
        subject = nil
        school = nil
        score = nil
        super.tearDown()
    }
    
    func testSchoolNameLabel_HasTheCorrectData() {
        XCTAssert(subject.schoolNameLabel.text == "Elmo's School")
    }
    
    func testOverviewLabel_HasTheCorrectData() {
        XCTAssert(subject.overviewLabel.text == "Hey, hey, hey... it's fat albert.")
    }
    
    func testNumberSATTakerLabel_HasTheCorrectData() {
        XCTAssert(subject.numberOfSatTakerLabel.text == "55")
    }
    
    func testCriticalReadingScoreLabel_HasTheCorrectData() {
        XCTAssert(subject.criticalReadingScoreLabel.text == "255")
    }
    
    func testMathScoreLabel_HasTheCorrectData() {
        XCTAssert(subject.mathScoreLabel.text == "7456")
    }
    
    func testWritingScoreLabel_HasTheCorrectData() {
        XCTAssert(subject.writingScoreLabel.text == "power level over 9000")
    }
}
