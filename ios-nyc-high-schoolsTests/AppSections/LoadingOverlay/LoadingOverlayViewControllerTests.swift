//
//  LoadingOverlayViewControllerTests.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import XCTest
@testable import ios_nyc_high_schools

class LoadingOverlayViewControllerTests: XCTestCase {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private var subject: LoadingOverlayViewController!
    
    
    override func setUp() {
        super.setUp()
        subject = UIStoryboard(identifier: .loadingOverlay).instantiateViewController()
        _ = subject.view
    }

    override func tearDown() {
        subject = nil
        super.tearDown()
    }
    
    func testViewDidLoad_ShowsTheAcitivutyIndicatorView() {
        XCTAssertFalse(subject.activityIndicatorView.isHidden)
    }
    
    func testViewDidLoad_InvokesStartAnimatingOnTheAcitivutyIndicatorView() {
        XCTAssertTrue(subject.activityIndicatorView.isAnimating)
    }
    
    func testLoadingIndicatorViewControllerCoordinatorRequestDismiss_InvokesStopAnimatingOnTheAcitivutyIndicatorView() {
        subject.loadingOverlayViewControllerCoordinatorRequestDimiss(LoadingOverlayViewControllerCoordinator(navigationController: nil))
        XCTAssertFalse(subject.activityIndicatorView.isAnimating)
    }
}
