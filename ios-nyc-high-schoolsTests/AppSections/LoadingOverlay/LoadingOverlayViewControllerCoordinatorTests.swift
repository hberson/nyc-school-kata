//
//  LoadingOverlayViewControllerCoordinatorTests.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import XCTest
@testable import ios_nyc_high_schools

class LoadingOverlayViewControllerCoordinatorTests: XCTestCase {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private var subject: LoadingOverlayViewControllerCoordinator!
    private var navigationController: MockNavigationController!
    private var delegate: MockLoadingOverlayDelegate!
    
    override func setUp() {
        super.setUp()
        navigationController = MockNavigationController()
        delegate = MockLoadingOverlayDelegate()
        
        subject = LoadingOverlayViewControllerCoordinator(navigationController: navigationController)
        subject.delegate = delegate
    }

    override func tearDown() {
        subject = nil
        navigationController = nil
        super.tearDown()
    }
    
    func testPresentLoadingOverlay_SetsTheViewControllerToPresentsModelPresentationStyleToOverCurrentContext() {
        subject.presentLoadinOverlay()
        
        let viewControllerToPresent = navigationController.viewControllerToPresent
        
        XCTAssertEqual(viewControllerToPresent?.modalPresentationStyle, UIModalPresentationStyle.overCurrentContext)
    }
    
    func testPresentLoadingOverlay_SetsPresentsTheCorrectViewController() {
        subject.presentLoadinOverlay()
        
        let viewControllerToPresent = navigationController.viewControllerToPresent
        
        XCTAssertTrue(viewControllerToPresent is LoadingOverlayViewController)
    }
    
    func testDismissLoadingOverlay_InvokesRequestDismiss() {
        subject.dismissLoadingOverlay()
        
        XCTAssertTrue(delegate.coordinator != nil)
        XCTAssert(delegate.loadingOverlayViewControllerCoordinatorRequestDimissCallCount == 1, "Expected loadingOverlayViewControllerCoordinatorRequestDimissCallCount to be called once, but got called \(delegate.loadingOverlayViewControllerCoordinatorRequestDimissCallCount) times.")
    }
}
