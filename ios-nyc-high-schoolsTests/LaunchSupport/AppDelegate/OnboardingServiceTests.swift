//
//  OnboardingServiceTests.swift
//  ios-nyc-high-schoolsTests
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import XCTest
@testable import ios_nyc_high_schools

class OnboardingServiceTests: XCTestCase {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private var subject: OnboardingService! = nil
    private var userDefaultsService: MockUserDefaultsService! = MockUserDefaultsService()
    
    override func setUp() {
        super.setUp()
        subject = OnboardingService(userDefaultsService: userDefaultsService)
    }

    override func tearDown() {
        subject = nil
        userDefaultsService = nil
        super.tearDown()
    }

    func test_givenTheUserHasNotInteractedWithOnboarding_whenApplicationDidFinishLaunchingWithOptions_thenTheRootViewControllerIsOnboardViewController() {
        _ = subject.application(UIApplication.shared, didFinishLaunchingWithOptions: [:])
        
        XCTAssertTrue(subject.window?.rootViewController is OnboardViewController)
        XCTAssertFalse(subject.window?.rootViewController is UINavigationController)
    }
    
    func test_givenTheUserHasInteractedWithOnboarding_whenApplicationDidFinishLaunchingWithOptions_thenTheRootViewControllerIsSchoolListViewController() {
        userDefaultsService.userHasInteractedwithOnboarding()
        
        _ = subject.application(UIApplication.shared, didFinishLaunchingWithOptions: [:])
        
        let navigationViewController = subject.window?.rootViewController as? UINavigationController
        let topViewController = navigationViewController?.topViewController
        
        XCTAssertTrue(topViewController is SchoolListViewController)
        XCTAssertFalse(subject.window?.rootViewController is OnboardViewController)
    }
}
