# NYC High School App Architecture
## Model View Controller + Coordinator (MVC + C)

I feel like Apple got it right with the aforementioned. With that said, if a project has already solified a design pattern and it's what is best for the team and codebase, then it is best to adhere to that. But, all of my personal projects review if [Apple's MVC architecture](https://developer.apple.com/library/archive/documentation/General/Conceptual/DevPedia-CocoaCore/MVC.html) would be a great fit analyzng alternative design patterns.

# Coordinator
I feel that a UIViewController does one thing well, which is control views. This is why I have adopted the Coordinator pattern which helps limit navigation code in view controllers. You can read more on this design pattern from the author [Soroush Khanlou](http://khanlou.com/2015/10/coordinators-redux/).

### Reasons:
* Keeps the barrier to entry for understanding the code low, minimize the amount of ramp-up time required to onboard new engineers.
* MVC is well dodumented

# Avoiding Massive View Controller
Massive view controllers only transpire when appropriate separation of concern isn't implemented. Honestly, by following the [SOLID design principles](https://drive.google.com/file/d/0ByOwmqah_nuGNHEtcU5OekdDMkk/view), you can make a 300 line view controller an uncommon occurrence.

# Table and Collection Views
I almost never use the Apple provided view controller templates (```UITableViewController``` & ```UICollectionViewController```). Although they can be useful for novice developers, they reduce your ability to properly adhere to separation of concerns and can be a challange to build on whenever you need to do non-default behavior.

# DataSources & Delegates
The data source and delegate method for table and collection views should not be implemented by the view controller. Instead, create seperate table/collection objects to manage its task. The aforementioned will prevent those details from polluting your view controller.

# Protocols
## Prefer Composition over Inheritance
[The Protocol-Oriented Programming in Swift session from WWDC 2015](https://developer.apple.com/videos/play/wwdc2015/408/) is a great entry to the power of protocols. Definitely worth a watch if you've never seen it or a re-watch for a quick refresher.

# Speed of thought (UI Responsiveness)
It is crucial that the app's user interface (UI) remain responsive during all aspect of the app's lifecycle. Normally, any delay/lag greater than 200 ms wil be immediately observeable by the user and could cause them to question wether or not the system registered their tap.

* Reserve the main thread for UI work.
* Avoid ignoring user interactions. There should almost never be a need to globally ignore user interaction by calling ```beginIgnoringInteractionEvents()``` on ```UIApplication```.

# Testing
Testing is a significant aspect of software development procress, which allows us to ship high quality software to production.

## Unit Testing
In its simplest form, a unit test corroborate wether a piece of code works as expected. A unit test will usually apply into one of three categories.

* Return Value Verification

      This is the simplest type of test to write

      1. Setup the system under test (SUT).
      2. Send the SUT a message.
      3. Verify the returned value is what you expected.


* State Verification

      This is the simplest type of test to write

      1. Setup the SUT with an inital state.
      2. Send the SUT a message that should mutate that state.
      3. Call an accessor method to get the state back and verify that it was mutated correctly.

* Behavior Verification

      This is the most challenging and involed type of test to write. Use this technique when you need to test something that performs side effects (i.e. Interacts with another underlying object). In this technique, you'll make use of a mock object to verify functionality.

      1. Setup the SUT with an inital state, inject mock object(s) via dependency injection.
      2. Send the SUT a message.
      3. Verify the mock object was called correct, in some cases its great to have an understading on how many times the mock object was called.


# NYC High School App Video Overview
![](kata-video.mov)

