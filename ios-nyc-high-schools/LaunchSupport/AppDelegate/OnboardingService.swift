//
//  OnboardingService.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

class OnboardingService: NSObject, AppDelegateService {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private let userDefaultsService: UserDefaultsServiceProtocol
    var window: UIWindow?
    
    private lazy var schoolListViewControllerCoordinator: SchoolListViewControllerCoordinator = {
       let coordinator = SchoolListViewControllerCoordinator()
        return coordinator
    }()
    
    private lazy var onboardViewControllerCoordinator: OnboardViewControllerCoordinator = {
        let coordinator = OnboardViewControllerCoordinator(window: self.window ?? UIWindow())
        return coordinator
    }()
    
    
    //------------------------
    // MARK: - Initialization
    //------------------------
    init(userDefaultsService: UserDefaultsServiceProtocol = UserDefaultsService()) {
        self.userDefaultsService = userDefaultsService
    }
    
    //------------------------
    // MARK: - Lifecycle
    //------------------------
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        if userDefaultsService.hasUserInteractedWithOnboarding {
            presentSchoolList()
        } else {
            presentOnboarding()
        }
        return true
    }
    
    //------------------------
    // MARK: - Helpers
    //------------------------
    private func presentSchoolList() {
        window?.rootViewController = schoolListViewControllerCoordinator.rootViewController
        window?.makeKeyAndVisible()
    }
    
    private func presentOnboarding() {
        window?.rootViewController = onboardViewControllerCoordinator.rootViewController
        window?.makeKeyAndVisible()
    }
}
