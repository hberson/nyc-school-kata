//
//  AppDelegate.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: NSObject, UIApplicationDelegate {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private(set) var services: [AppDelegateService] = [OnboardingService()]
    
    //------------------------
    // MARK: - Initialization
    //------------------------
    init(services: [AppDelegateService]) {
        self.services = services
        super.init()
    }
    
    override init() {
        super.init()
    }
    
    //------------------------
    // MARK: - Lifecycle
    //------------------------
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        var results: [Bool] = []
        
        for service in services {
            if let result = service.application?(application, didFinishLaunchingWithOptions: launchOptions) {
                results.append(result)
            }
        }
        return results.filter({ $0 == true }).count == results.count
    }
}
