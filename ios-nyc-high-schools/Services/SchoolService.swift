//
//  SchoolService.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import Foundation

enum ServiceResponse<T> {
    case success(T)
    case error(Error)
}

enum ServiceRequestErrorType: Error {
    case invalidPath
}

extension ServiceRequestErrorType: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .invalidPath:
            return NSLocalizedString("Invalid url path, please try again.", comment: "")
        }
    }
}

class SchoolService: SchoolServiceProtocol {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private let session: URLSessionProtocol
    private let mainThread = OperationQueue.main
    
    //------------------------
    // MARK: - Initialization
    //------------------------
    init(session: URLSessionProtocol = URLSession.shared) {
        self.session = session
    }
    
    //------------------------
    // MARK: - Helpers
    //------------------------
    func makeRequest<T: Decodable>(for path: String, handler: @escaping (ServiceResponse<[T]>) -> ()) {
        guard let url = URL(string: path) else {
            handler(.error(ServiceRequestErrorType.invalidPath))
            return
        }
        
        let dataTask = session.dataTask(with: url) { [weak self] (data, response, error) in
            self?.mainThread.addOperation {
                if let error = error {
                    handler(.error(error))
                } else if let data = data {
                    do {
                        handler(.success(try JSONDecoder().decode([T].self, from: data)))
                    } catch {
                        handler(.error(error))
                    }
                }
            }
        }
        
        dataTask.resume()
    }
}
