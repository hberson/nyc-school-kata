//
//  StoryboardIdentifiable.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

protocol StoryboardIdentifiable {
    
    static var StoryboardIdentifier: String { get }
}

extension UIViewController: StoryboardIdentifiable {
    
    static var StoryboardIdentifier: String {
        return String(describing: self)
    }
}
