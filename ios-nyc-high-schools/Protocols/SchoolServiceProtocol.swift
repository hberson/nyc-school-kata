//
//  SchoolServiceProtocol.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import Foundation

protocol SchoolServiceProtocol {
    func makeRequest<T: Decodable>(for path: String, handler: @escaping (ServiceResponse<[T]>) -> ())
}
