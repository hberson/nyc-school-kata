//
//  UserDefaultsServiceProtocol.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import Foundation

protocol UserDefaultsServiceProtocol {
    var hasUserInteractedWithOnboarding: Bool { get }
    
    func userHasInteractedwithOnboarding()
}
