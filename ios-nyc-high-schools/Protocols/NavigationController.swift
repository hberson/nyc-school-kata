//
//  NavigationController.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

protocol NavigationController: AnyObject {
    var interactivePopGestureRecognizer: UIGestureRecognizer? { get }
    var viewControllers: [UIViewController] { get }
    
    func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)?)
    func pushViewController(_ viewController: UIViewController, animated: Bool)
    func setNavigationBarHidden(_ hidden: Bool, animated: Bool)
}

extension UINavigationController: NavigationController {}
