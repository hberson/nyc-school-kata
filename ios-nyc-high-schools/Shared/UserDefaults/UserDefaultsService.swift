//
//  UserDefaultsService.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import Foundation

private enum UserDefaultsKey {
    static let key = "UserDefaultsKey.hasUserInteractedWithOnboardingScreens"
}

class UserDefaultsService: UserDefaultsServiceProtocol {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private let userDefaults: UserDefaultsProtocol
    
    var hasUserInteractedWithOnboarding: Bool {
        return userDefaults.bool(forKey: UserDefaultsKey.key)
    }
    
    //------------------------
    // MARK: - Initialization
    //------------------------
    init(userDefaults: UserDefaultsProtocol = UserDefaults.standard) {
        self.userDefaults = userDefaults
    }
    
    //------------------------
    // MARK: - Helpers
    //------------------------
    func userHasInteractedwithOnboarding() {
        userDefaults.set(true, forKey: UserDefaultsKey.key)
    }
}
