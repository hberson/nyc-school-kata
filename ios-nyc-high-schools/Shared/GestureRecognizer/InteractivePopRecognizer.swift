//
//  InteractivePopRecognizer.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

class InteractivePopRecognizer: NSObject {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    unowned var navigationController: NavigationController
    
    //------------------------
    // MARK: - Initialization
    //------------------------
    init(navigationController: NavigationController) {
        self.navigationController = navigationController
    }
}

//------------------------
// MARK: - UIGestureRecognizerDelegate
//-----------------------
extension InteractivePopRecognizer: UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return !navigationController.viewControllers.isEmpty
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
