//
//  UIView+Extensions.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

extension UIView {
    
    func applyShadow() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowRadius = 1.0
        layer.shadowOpacity = 0.77
    }
    
    func applyRoundedCorners(cornerRadius: CGFloat) {
        layer.cornerRadius = cornerRadius
    }
    
    func applyMask() {
        layer.masksToBounds = true
    }
}
