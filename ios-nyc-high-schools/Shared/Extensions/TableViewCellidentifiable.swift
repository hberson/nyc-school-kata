//
//  TableViewCellidentifiable.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

protocol TableViewCellidentifiable {
    
    static var CellResuseIdentifier: String { get }
}

extension UITableViewCell: TableViewCellidentifiable {
    
    static var CellResuseIdentifier: String {
        return String(describing: self)
    }
}
