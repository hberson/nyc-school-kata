//
//  UIStoryboard+Extensions.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    struct Identifier {
        let name: String
        let bundle: Bundle
        
        init(name: String, bundle: Bundle = Bundle.main) {
            self.name = name
            self.bundle = bundle
        }
    }
    
    convenience init(identifier: Identifier) {
        self.init(name: identifier.name, bundle: identifier.bundle)
    }
    
    func instantiateViewController<T: UIViewController>() -> T {
        guard let viewController = instantiateViewController(withIdentifier: T.StoryboardIdentifier) as? T else {
            fatalError("unable to instantiate view controller with identifier \(T.StoryboardIdentifier) from storyboard \(self)")
        }
        return viewController
    }
}
