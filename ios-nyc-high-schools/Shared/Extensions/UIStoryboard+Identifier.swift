//
//  UIStoryboard+Identifier.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

extension UIStoryboard.Identifier {
    
    static let schoolList = UIStoryboard.Identifier(name: "SchoolList")
    static let onboard = UIStoryboard.Identifier(name: "Onboard")
    static let loadingOverlay = UIStoryboard.Identifier(name: "LoadingOverlay")
    static let detailView = UIStoryboard.Identifier(name: "DetailView")
}
