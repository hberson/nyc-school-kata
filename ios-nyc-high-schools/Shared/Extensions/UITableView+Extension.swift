//
//  UITableView+Extension.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

extension UITableView {
    
    func dequeueReusableCell<T: UITableViewCell>() -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.CellResuseIdentifier) as? T else {
            fatalError("Unable to dequeue reusable cell with identifier \(T.CellResuseIdentifier)")
        }
        return cell
    }
}
