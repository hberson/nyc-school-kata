//
//  SchoolListDataSource.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/27/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

class SchoolListDataSource: NSObject {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private var schools: [School] = []
    
    //------------------------
    // MARK: - Initialization
    //------------------------
    init(schools: [School]) {
        self.schools = schools
    }
}

//------------------------
// MARK: - UITableViewDataSource
//------------------------
extension SchoolListDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return schools.isEmpty ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SchoolCell = tableView.dequeueReusableCell()
        cell.configureCellFor(schools[indexPath.item])
        return cell
    }
}
