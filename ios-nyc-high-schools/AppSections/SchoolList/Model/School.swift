//
//  School.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import Foundation

struct School: Decodable {
    let dbn: String?
    let schoolName: String?
    let overviewParagraph: String?
    let academicopportunities1: String?
    let academicopportunities2: String?
    let ellPrograms: String?
    let neighborhood: String?
    let phoneNumber: String?
    let faxNumber: String?
    let schoolEmail: String?
    let website: String?
    let totalStudents: String?
    let extracurricularActivities: String?
    let schoolSports: String?
    let primaryAddressLine1: String?
    let city: String?
    let zip: String?
    let stateCode: String?
    var score: Score?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case academicopportunities1
        case academicopportunities2
        case ellPrograms = "ell_programs"
        case neighborhood
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case website
        case totalStudents = "total_students"
        case extracurricularActivities = "extracurricular_activities"
        case schoolSports = "school_sports"
        case primaryAddressLine1 = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
    }
}
