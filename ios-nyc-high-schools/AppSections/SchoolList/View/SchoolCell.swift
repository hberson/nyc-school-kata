//
//  SchoolCell.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

class SchoolCell: UITableViewCell {
    
    func configureCellFor(_ school: School) {
        textLabel?.text = school.schoolName
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        textLabel?.text = nil
    }
}
