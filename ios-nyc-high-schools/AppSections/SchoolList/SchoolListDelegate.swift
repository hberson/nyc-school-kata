//
//  SchoolListDelegate.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/27/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

protocol SchoolListSelectionFlowDelegate: AnyObject {
    func schoolListDelegateDidSelect(_ delegate: SchoolListDelegate, school: School)
}

class SchoolListDelegate: NSObject {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    weak var delegate: SchoolListSelectionFlowDelegate?
    private var schools: [School] = []
    private var scores: [Score] = []
    
    //------------------------
    // MARK: - Initialization
    //------------------------
    init(schools: [School], scores: [Score]) {
        self.schools = schools
        self.scores = scores
    }
}

//------------------------
// MARK: - UITableViewDelegate
//------------------------
extension SchoolListDelegate: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard !schools.isEmpty else {
            return
        }
        
        var school = schools[indexPath.item]
        if let score = scores.first(where: { $0.dbn == school.dbn }) {
            school.score = score
        }
        delegate?.schoolListDelegateDidSelect(self, school: school)
    }
}
