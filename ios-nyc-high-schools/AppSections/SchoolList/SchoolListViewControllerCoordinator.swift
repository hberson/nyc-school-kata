//
//  SchoolListViewControllerCoordinator.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

protocol SchoolListViewControllerCoordinatorDelegate: AnyObject {
    func SchoolListViewControllerCoordinatorServiceRequestCompleted(schools: [School], scores: [Score])
}

class SchoolListViewControllerCoordinator {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    weak var delegate: SchoolListViewControllerCoordinatorDelegate?
    private var popRecognizer: InteractivePopRecognizer?
    private let schoolListApiEndpoint = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    private let schoolListScoresEndpoint = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    private var schools: [School] = []
    private var scores: [Score] = []
    private var service: SchoolServiceProtocol
    private var navigationController: NavigationController?
    private let dispathGroup: DispatchGroup = DispatchGroup()
    
    private lazy var schoolListViewController: SchoolListViewController = {
        let controller: SchoolListViewController = UIStoryboard(identifier: .schoolList).instantiateViewController()
        controller.delegate = self
        self.delegate = controller
        return controller
    }()
    
    private lazy var loadingOverlayViewControllerCoordinator: LoadingOverlayViewControllerCoordinator = {
        let coordinator: LoadingOverlayViewControllerCoordinator = LoadingOverlayViewControllerCoordinator(navigationController: self.navigationController)
        return coordinator
    }()
    
    
    var rootViewController: UINavigationController? {
        let navigationController = UINavigationController(rootViewController: schoolListViewController)
        navigationController.setNavigationBarHidden(true, animated: false)
        self.navigationController = navigationController
        setInteractiveRecognizer()
        return navigationController
    }
    
    init(service: SchoolServiceProtocol = SchoolService()) {
        self.service = service
    }
    
    //------------------------
    // MARK: - Helpers
    //------------------------
    private func requestNYCSchoolList() {
        dispathGroup.enter()
        service.makeRequest(for: schoolListApiEndpoint) { [weak self] (result: ServiceResponse<[School]>) in
            switch result {
            case let .success(schools):
                self?.schools = schools
            case let .error(error):
                print(error.localizedDescription)
            }
            self?.dispathGroup.leave()
        }
    }
    
    private func requestNYCSchoolSAT() {
        dispathGroup.enter()
        service.makeRequest(for: schoolListScoresEndpoint) { [weak self] (result: ServiceResponse<[Score]>) in
            switch result {
            case let .success(scores):
                self?.scores = scores
            case let .error(error):
                print(error.localizedDescription)
            }
            self?.dispathGroup.leave()
        }
    }
    
    private func updateData() {
        dispathGroup.notify(queue: DispatchQueue.main) { [unowned self] in
            self.loadingOverlayViewControllerCoordinator.dismissLoadingOverlay()
            self.delegate?.SchoolListViewControllerCoordinatorServiceRequestCompleted(schools: self.schools, scores: self.scores)
        }
    }
    
    private func setInteractiveRecognizer() {
        guard let navigationController = self.navigationController else { return }
        popRecognizer = InteractivePopRecognizer(navigationController: navigationController)
        navigationController.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
}

extension SchoolListViewControllerCoordinator: SchoolListViewControllerDelegate {
    
    func schoolListViewControllerFetchLastestInfo(_ viewController: SchoolListViewController) {
        loadingOverlayViewControllerCoordinator.presentLoadinOverlay()
        requestNYCSchoolList()
        requestNYCSchoolSAT()
        updateData()
    }
    
    func schoolListViewControllerPresentDetailView(_ viewController: SchoolListViewController, school: School) {
        let destination: DetailViewController = UIStoryboard(identifier: .detailView).instantiateViewController()
        destination.school = school
        navigationController?.pushViewController(destination, animated: true)
    }
}
