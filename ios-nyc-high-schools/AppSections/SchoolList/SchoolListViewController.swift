//
//  SchoolListViewController.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

protocol SchoolListViewControllerDelegate: AnyObject {
    func schoolListViewControllerFetchLastestInfo(_ viewController: SchoolListViewController)
    func schoolListViewControllerPresentDetailView(_ viewController: SchoolListViewController, school: School)
}

class SchoolListViewController: UIViewController {
    //------------------------
    // MARK: - IB Properties
    //------------------------
    @IBOutlet private(set) var tableView: UITableView!
    
    //------------------------
    // MARK: - Member Variables
    //------------------------
    weak var delegate: SchoolListViewControllerDelegate?
    private(set) var schoolListDataSource: SchoolListDataSource?
    private(set) var schoolListDelegate: SchoolListDelegate?
    
    //------------------------
    // MARK: - Lifecycle
    //------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        delegate?.schoolListViewControllerFetchLastestInfo(self)
    }
    
    //------------------------
    // MARK: - Helpers
    //------------------------
    private func configureTableView() {
        tableView.rowHeight = 60
    }
}

//------------------------
// MARK: - SchoolListViewControllerCoordinatorDelegate
//------------------------
extension SchoolListViewController: SchoolListViewControllerCoordinatorDelegate {
    
    func SchoolListViewControllerCoordinatorServiceRequestCompleted(schools: [School], scores: [Score]) {
        schoolListDataSource = SchoolListDataSource(schools: schools)
        schoolListDelegate = SchoolListDelegate(schools: schools, scores: scores)
        schoolListDelegate?.delegate = self
        
        tableView.dataSource = schoolListDataSource
        tableView.delegate = schoolListDelegate
        
        tableView.reloadData()
    }
}

//------------------------c
// MARK: - SchoolListViewControllerCoordinatorDelegate
//------------------------
extension SchoolListViewController: SchoolListSelectionFlowDelegate {
    
    func schoolListDelegateDidSelect(_ delegate: SchoolListDelegate, school: School) {
        self.delegate?.schoolListViewControllerPresentDetailView(self, school: school)
    }
}
