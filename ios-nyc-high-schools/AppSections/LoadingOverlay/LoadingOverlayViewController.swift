//
//  LoadingOverlayViewController.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

class LoadingOverlayViewController: UIViewController {
    //------------------------
    // MARK: - IB Properties
    //------------------------
    @IBOutlet private(set) var activityIndicatorView: UIActivityIndicatorView!
    //------------------------
    // MARK: - Lifecycle
    //------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicatorView.isHidden = false
        activityIndicatorView.startAnimating()
    }
}

//------------------------
// MARK: - LoadingOverlayViewControllerCoordinatorDelegate
//------------------------
extension LoadingOverlayViewController: LoadingOverlayViewControllerCoordinatorDelegate {
    
    func loadingOverlayViewControllerCoordinatorRequestDimiss(_ coordinator: LoadingOverlayViewControllerCoordinator) {
        activityIndicatorView.stopAnimating()
        dismiss(animated: true, completion: nil)
    }
}
