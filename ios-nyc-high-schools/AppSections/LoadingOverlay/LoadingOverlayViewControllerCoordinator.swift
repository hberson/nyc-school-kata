//
//  LoadingOverlayViewControllerCoordinator.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

protocol LoadingOverlayViewControllerCoordinatorDelegate: AnyObject {
    func loadingOverlayViewControllerCoordinatorRequestDimiss(_ coordinator: LoadingOverlayViewControllerCoordinator)
}

class LoadingOverlayViewControllerCoordinator {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    weak var delegate: LoadingOverlayViewControllerCoordinatorDelegate?
    private let navigationController: NavigationController?
    
    private lazy var loadingOverlayViewController: LoadingOverlayViewController = {
        let controller: LoadingOverlayViewController = UIStoryboard(identifier: .loadingOverlay).instantiateViewController()
        self.delegate = controller
        return controller
    }()
    
    //------------------------
    // MARK: - Initialization
    //------------------------
    init(navigationController: NavigationController?) {
        self.navigationController = navigationController
    }
    
    //------------------------
    // MARK: - Helpers
    //------------------------
    func presentLoadinOverlay() {
        loadingOverlayViewController.modalPresentationStyle = .overCurrentContext
        navigationController?.present(loadingOverlayViewController, animated: true, completion: nil)
    }
    
    func dismissLoadingOverlay() {
        delegate?.loadingOverlayViewControllerCoordinatorRequestDimiss(self)
    }
}
