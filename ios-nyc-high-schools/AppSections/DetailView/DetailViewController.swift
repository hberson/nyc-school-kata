//
//  DetailViewController.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/26/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    //------------------------
    // MARK: - IB Properties
    //------------------------
    @IBOutlet private(set) var schoolNameLabel: UILabel!
    @IBOutlet private(set) var overviewLabel: UILabel!
    @IBOutlet private(set) var numberOfSatTakerLabel: UILabel!
    @IBOutlet private(set) var criticalReadingScoreLabel: UILabel!
    @IBOutlet private(set) var mathScoreLabel: UILabel!
    @IBOutlet private(set) var writingScoreLabel: UILabel!
    
    //------------------------
    // MARK: - Member Variables
    //------------------------
    var school: School?
    
    //------------------------
    // MARK: - Lifecycle
    //------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        confiureDetails()
    }
    
    //------------------------
    // MARK: - Helpers
    //------------------------
    private func confiureDetails() {
        schoolNameLabel.text = school?.schoolName
        overviewLabel.text = school?.overviewParagraph
        
        guard let score = school?.score else { return }
        
        numberOfSatTakerLabel.text = score.numOfSatTestTakers
        criticalReadingScoreLabel.text = score.satCriticalReadingAvgScore
        mathScoreLabel.text = score.satMathAvgScore
        writingScoreLabel.text = score.satWritingAvgScore
    }
}
