//
//  OnboardViewControllerCoordinator.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

class OnboardViewControllerCoordinator {
    //------------------------
    // MARK: - Member Variables
    //------------------------
    private let window: UIWindow
    private lazy var onboardViewController: OnboardViewController = {
        let controller: OnboardViewController = UIStoryboard(identifier: .onboard).instantiateViewController()
        controller.delegate = self
        return controller
    }()
    
    private lazy var schoolListViewControllerCoordinator: SchoolListViewControllerCoordinator = {
        let coordinator = SchoolListViewControllerCoordinator()
        return coordinator
    }()
    
    var rootViewController: OnboardViewController {
        return onboardViewController
    }
    
    //------------------------
    // MARK: - Initialization
    //------------------------
    init(window: UIWindow) {
        self.window = window
    }
    
    //------------------------
    // MARK: - Helpers
    //------------------------
    private func presentSchoolList() {
        UIView.transition(with: window, duration: 0.5, options: .transitionCrossDissolve, animations: { [unowned self] in
            self.window.rootViewController = self.schoolListViewControllerCoordinator.rootViewController
        }, completion: nil)
    }
}

//------------------------
// MARK: - OnboardViewControllerDelegate
//------------------------
extension OnboardViewControllerCoordinator: OnboardViewControllerDelegate {
    
    func onboardViewControllerDidTapOkGotIt(_ viewController: OnboardViewController) {
        presentSchoolList()
    }
}
