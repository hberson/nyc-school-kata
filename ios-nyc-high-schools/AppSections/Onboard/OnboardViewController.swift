//
//  OnboardViewController.swift
//  ios-nyc-high-schools
//
//  Created by Henry B Saint-Juste on 6/25/19.
//  Copyright © 2019 TheAppDoctor. All rights reserved.
//

import UIKit

protocol OnboardViewControllerDelegate: AnyObject {
    func onboardViewControllerDidTapOkGotIt(_ viewController: OnboardViewController)
}

class OnboardViewController: UIViewController {
    //------------------------
    // MARK: - IB Properties
    //------------------------
    @IBOutlet private(set) var okButton: UIButton!
    
    //------------------------
    // MARK: - Member Variables
    //------------------------
    weak var delegate: OnboardViewControllerDelegate?
    var userDefaultsService: UserDefaultsServiceProtocol = UserDefaultsService()
    
    //------------------------
    // MARK: - Lifecycle
    //------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        okButton.applyShadow()
        okButton.applyRoundedCorners(cornerRadius: 10)
    }
    
    //------------------------
    // MARK: - Actions
    //------------------------
    @IBAction private func okGotItButtonAction() {
        userDefaultsService.userHasInteractedwithOnboarding()
        delegate?.onboardViewControllerDidTapOkGotIt(self)
    }
}
